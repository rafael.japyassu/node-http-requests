import { viaCepApi } from "./viaCep";
import { randomUUID } from 'crypto';

export class ZipCodeService {
  constructor() {}

  async findAddressByZipCode(zipCode: string) {
    // console.log('Antes do request:');
    // console.log(viaCepApi.defaults.headers);
    const result = await viaCepApi.get(`/ws/${zipCode}/json`);

    // JWT

    viaCepApi.defaults.headers.options = {
      'Authorization': `Bearer ${randomUUID()}`
    }

    // console.log(viaCepApi.defaults.headers);

    return result;
  }
}