import { createServer, IncomingMessage } from 'http';
import { indexRoutes } from './routes';
import { peopleRoutes } from './routes/peoples';
import { zipCodeRoutes } from './routes/zipCode';
import { HttpMethods } from './types';
import { addRoute, convertParam, routes } from './utils';

addRoute('/', indexRoutes);
addRoute('/peoples', peopleRoutes);
addRoute('/zip-codes/:cep', zipCodeRoutes)

const server = createServer(async (request, response) => {
  const { url, method } = request;
  const httpMethod = method as HttpMethods;

  if (url && method) {
    const routeKeys = Object.keys(routes);
    console.log(url);
    const params = routeKeys.map(route => {
      console.log(route);
      return convertParam(route, url, request);
    }).filter(Boolean);

    const route = (Array.isArray(params) && params?.length > 0) ? params[0]?.route || url : url;
    const service = routes[route][httpMethod];

    if (service) {
      if (params[0]) {
        const { route, ...rest } = params[0];
  
        service.handle(request, response, rest);
      } else {
        service.handle(request, response);
      }

    }
  }
});

server.listen(3000, () => {
  console.log('API running in http://localhost:3000');
});
