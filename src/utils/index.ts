import { ServerResponse, IncomingMessage } from 'http';
import https from "https";
import { Call, Route } from "../types";

export const routes = {} as Route;

export function addRoute(router: string, call: Call) {
  routes[router] = call;
}

export function configResponse(
  statusCode: number,
  response: ServerResponse,
  json: string = "",
  headers = {'Content-Type': 'application/json'}
) {
  if (statusCode === 204) {
    response.writeHead(statusCode);
  } else {
    response.writeHead(statusCode, headers);
    response.write(json);
  }
  response.end();
}

async function toBodyPromise<T>(request: IncomingMessage): Promise<T> {
  return new Promise((resolve) => {
    let body = '';

    request.on('data', (chunck) => {
      body += chunck;
    });

    request.on('end', () => {
      const data = JSON.parse(body);
      resolve(data);
    });
  });
}

export function convertParam(route: string, url: string, request: IncomingMessage) {
  const splitUrl = url.split('/');
  const splitRoute = route.split('/');

  if (splitRoute.length === splitUrl.length) {
    const key = splitRoute[splitRoute.length - 1].replace(':', '');
    const value =  splitUrl[splitUrl.length - 1];

    const param = {
      [key]: value,
      route,
    };

    return param;
  } else {
    return null;
  }
}

export async function clientRequestPromise<T>(url: string) {
  return new Promise((resolve, reject) => {
    const req = https.request(url, async (res) => {
      try {
        const data = await toBodyPromise<T>(res);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });

    req.on('error', (error) => {
      reject(error);
    });
    
    req.end();
  });
}