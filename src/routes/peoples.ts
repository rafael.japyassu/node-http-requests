import { Call } from "../types";
import { configResponse } from "../utils";
import { peoples } from "../database";
import { People, Response } from '../types'

export const peopleRoutes: Call = {
  GET: {
    handle(request, response) {
      const json: Response<People[]> = {
        success: true,
        data: peoples,
        errors: []
      };

      configResponse(
        200,
        response,
        JSON.stringify(json)
      );
    },
  }
}