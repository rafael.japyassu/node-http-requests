import { Call } from "../types";
import { configResponse } from "../utils";
import https from 'https';
import axios from 'axios';
import { api } from "../services/api";
import { ZipCodeService } from "../services/zipCode";

async function toPromise(url: string) {
  return new Promise((resolve, reject) => {
    const req = https.request(url, (res) => {
      let data = '';
  
      res.on('data', (chunck) => {
        data += chunck;
      });
  
      res.on('end', () => {
        try {
          const payload = JSON.parse(data);
          resolve(payload);
        } catch (err) {
          reject(err);
        }
      });
    });
  
    req.end();
  });
}

export const zipCodeRoutes: Call = {
  GET: {
    async handle(request, response, params) {

      console.log('Params:');
      console.log(params);

      // const result = await toPromise('https://viacep.com.br/ws/01001000/json');
      const viaCepService = new ZipCodeService();
      const result = await viaCepService.findAddressByZipCode("01001000");
      const data = {
        name: 'Mouse da Dell 2'
      }

      // const res = await axios.post('http://localhost:3001/products', data);
      // const res = await api.put('/products/2', data);
      // await api.delete('/products/2');

      // console.log(res)

      configResponse(
        result.status,
        response,
        JSON.stringify(result.data)
      );
    },
  }
}