import { IncomingMessage, ServerResponse } from 'http';

export type RequestParams = {
  [key: string]: unknown;
}

export interface ICallAction {
  handle(request: IncomingMessage, response: ServerResponse, params?: RequestParams): void | Promise<void>;
}

export type HttpMethods = 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';

export type Call = {
  [key in HttpMethods]?: ICallAction;
}

export type Route = {
  [key: string]: Call;
};

export type People = {
  name: string;
  age: number;
  cpf?: string;
}

export type Response<T> = {
  success: boolean;
  data: T | null | undefined;
  errors: string[];
}