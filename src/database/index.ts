import { People } from "../types";

export const peoples: People[] = [
  {
    name: 'Bianca',
    age: 19,
    cpf: '111.111.111-11'
  },
  {
    name: 'João',
    age: 20,
    cpf: '222.222.222-22'
  },
  {
    name: 'Maria',
    age: 21,
    cpf: '333.333.333-33'
  },
  {
    name: 'Teste',
    age: 22,
    cpf: '444.444.444-44'
  },
];